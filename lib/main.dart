import 'package:flutter/material.dart';

const String firstHome = '/';
const String secondHome = '/second';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        initialRoute: firstHome,
//        onGenerateRoute: (RouteSettings settings) {
//          switch (settings.name) {
//            case '/':
//              return SlideRightRoute(widget: FirstPage());
//              break;
//            case '/second':
//              User user = settings.arguments;
//              return SlideRightRoute(widget: UserPage(user));
//              break;
//          }
//        },
        routes: <String, WidgetBuilder>{
          firstHome: (context) => FirstPage(),
          secondHome: (context) => UserPage(),
        },
        onUnknownRoute: (RouteSettings setting) {
          String unknownRoute = setting.name;
          return new MaterialPageRoute(
              builder: (context) => NotFoundPage(unknownRoute));
        });
  }
}

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: SafeArea(
        child: Center(
          child: RaisedButton(
            onPressed: () {
              User denis = User(name: 'Denis', age: 27);
              Navigator.of(context).pushNamed(secondHome, arguments: denis);
            },
            child: Text('Go to user\'s page'),
          ),
        ),
      ),
    );
  }
}

class UserPage extends StatelessWidget {
//  final User user;
//
//  UserPage(this.user);

  @override
  Widget build(BuildContext context) {
    final User user = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text('${user.name}\'s Page'),
      ),
      body: SafeArea(
        child: Center(
          child: RaisedButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Back to first'),
          ),
        ),
      ),
    );
  }
}

class NotFoundPage extends StatelessWidget {
  final String unknownRoute;

  NotFoundPage(this.unknownRoute);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second'),
      ),
      body: SafeArea(child: Text('Page not found ($unknownRoute)')),
    );
  }
}

class SlideRightRoute extends PageRouteBuilder {
  final Widget widget;

  SlideRightRoute({this.widget})
      : super(
          pageBuilder: (BuildContext context, Animation<double> animation,
              Animation<double> secondaryAnimation) {
            return widget;
          },
          transitionsBuilder: (BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
              Widget child) {
            return new SlideTransition(
              position: new Tween<Offset>(
                begin: const Offset(1.0, 0.0),
                end: Offset.zero,
              ).animate(animation),
              child: child,
            );
          },
        );
}

class User {
  final String name;
  final int age;

  User({this.name, this.age});
}
